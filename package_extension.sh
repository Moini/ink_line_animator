#!/bin/bash

today=$(date +%Y.%m.%d.%H.%M)
zipfilename="/tmp/ink_line_animator_$today.zip"

zip -r $zipfilename ./* -x .git* -x package_extension.sh

gpg --detach-sign --armor -u maren@goos-habermann.de -o $zipfilename.sig $zipfilename

gpg --verify $zipfilename.sig $zipfilename
