<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">

  <name>Line Animator</name>
  <id>de.vektorrascheln.line_animator</id>

  <dependency type="executable" location="inx">line-animator.py</dependency>

  <hbox><spacer size="expand"/>
    <param name="inkversion" type="optiongroup" gui-description="See Help -> About Inkscape" appearance="combo"
  gui-text="Inkscape version">
    <option value="1.3+">1.3+</option>
    <option value="1.2">1.2</option>
    <option value="1.1">1.1</option>
    </param>
  </hbox>

  <param name="action" type="notebook">
    <page name="add_anim" gui-text="Add Animation">
      <param name="add_desc" type="description">Animate the selected objects as if they were drawn with a pencil.</param>
      <param name="identifier" type="string" gui-description="Only letters A-Z and a-z, numbers and underscores." gui-text="Unique name:">animation_1</param>
      <param name="duration" type="float" precision="3" min="0.001" max="1000.000" gui-description="Duration of the animation in seconds." gui-text="Duration (seconds):">10.000</param>
      <param name="repeat"  type="int"   min="0" max="1000" gui-description="Number of times the animation should be played. 0 means infinite repetition." gui-text="Number of repetitions (0 = infinite):">1</param>
      <param name="delay" type="float" precision="3" min="0.000" max="1000.000" gui-description="Time to wait until the animation starts. Can be used to chain animations with different speed, or to create parallel animations." gui-text="Delay (seconds):">0.000</param>
    </page>
    <page name="advanced" gui-text="Advanced options">
      <param name="timing" type="enum" gui-description="Select the timing profile of the animation" gui-text="Timing function">
        <item value="ease">ease</item>
        <item value="ease-in">ease-in</item>
        <item value="ease-out">ease-out</item>
        <item value="ease-in-out">ease-in-out</item>
        <item value="linear">linear</item>
      </param>
    </page>
    <page name="remove_anim" gui-text="Remove Animation">
      <param name="remove_from" type="enum" gui-description="Remove all traces of animations from the file or only remove animations from selected objects." gui-text="Remove animations: ">
        <item value="selected">from selected objects</item>
        <item value="all">from the whole document</item>
      </param>
    </page>
    <page name="help" gui-text="Help">
      <param name="help_text_0" type="description">This extension allows you to convert your line drawings into CSS animations that will run on any modern web browser. The animations will look as if the paths are drawn by hand.</param>
      <param name="help_text_1" type="description">Set color and stroke width to your liking. Subpaths will be drawn simultaneously. Separate paths will be drawn in stacking order.</param>
      <param name="help_text_2" type="description">You can use the extension multiple times per document for (entirely) different sets of objects. This allows you to set different durations, and optionally adding a delay, so the separate animations will start at different times.</param>
      <param name="help_text_3" type="description">Extension development happens on GitLab at:</param>
      <param name="help_text_4" type="description" appearance="url">https://gitlab.com/Moini/ink_line_animator</param>
    </page>
  </param>


  <effect needs-live-preview="false" >
    <object-type>path</object-type>
    <effects-menu>
      <submenu name="Animation"/>
    </effects-menu>
    <icon>ink_line_animator_70x60.svg</icon>
  </effect>

  <script>
    <command reldir="inx" interpreter="python">line-animator.py</command>
  </script>

</inkscape-extension>
